# Insomnia Website

This is the source code for [insomnia.rest](https://insomnia.rest), built with
[Hugo Static Website Engine](https://gohugo.io/) and 
[Speedpack](https://github.com/gschier/speedpack) to compress static assets on
deploy.

```shell
# Run Development Server
npm start
```
